<# This is run along with Task Scheduler at logon one time to make local remote user administrator
Read WVD Shared Notes One Note notebook on how to setup Scheduled task
#>

# Get user from Remote Desktop Users group and then add to Administrators group
$localuser = Get-LocalGroupMember -Group "Remote Desktop Users" | Select-Object -ExpandProperty Name 
Add-LocalGroupMember -Group "Administrators" -Member $localuser 

# Remove Scheduled task after Adding Local Administrator 
if ($(Get-ScheduledTask -TaskName "RemoteUserToAdmin" -ErrorAction SilentlyContinue).TaskName -eq "RemoteUserToAdmin") {
    Unregister-ScheduledTask -TaskName "RemoteUserToAdmin" -Confirm:$False
}